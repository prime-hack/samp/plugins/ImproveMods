option(${PROJECT_NAME}_MAP "Generate .map file [ON/OFF]" OFF)

find_program(UPX_BIN upx)
if(UPX_BIN)
	if (${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
		option(${PROJECT_NAME}_UPX "Execute UPX after build [ON/OFF]" OFF)
	else()
		option(${PROJECT_NAME}_UPX "Execute UPX after build [ON/OFF]" ON)
	endif()
endif()

if (${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang" OR MSVC)
	option(${PROJECT_NAME}_PDB "Generate .pdb file [ON/OFF]" ON)
endif()

find_program(LLVM_LLD_BIN lld)
if (LLVM_LLD_BIN)
	if (${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
		option(${PROJECT_NAME}_LLD "Use lld linker from llvm [ON/OFF]" ON)
	else()
		option(${PROJECT_NAME}_LLD "Use lld linker from llvm [ON/OFF]" OFF)
	endif()
endif()
