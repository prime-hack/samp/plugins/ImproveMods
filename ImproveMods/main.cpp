#include "main.h"

#include <cstring>
#include <fstream>
#include <set>

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	CdStreamOpen_.onBefore += std::tuple{ this, &AsiPlugin::CdStreamOpen };
	CdStreamOpen_.install( 4, 0, false );

	wchar_t system_exe_path[MAX_PATH];
	GetModuleFileNameW( GetModuleHandle( nullptr ), system_exe_path, MAX_PATH );
	mods_path = std::filesystem::path( system_exe_path ).parent_path() / "mods";

	if ( !std::filesystem::exists( mods_path ) ) std::filesystem::create_directories( mods_path );

	for ( auto &&entry : std::filesystem::directory_iterator( mods_path ) ) {
		if ( !entry.is_regular_file() ) continue;

		const auto &file = entry.path();
		if ( !file.has_extension() || !file.has_stem() ) continue;

		auto ext = file.extension().string();
		std::transform( ext.begin(), ext.end(), ext.begin(), ::tolower );
		if ( ext != ".dff" && ext != ".txd" && ext != ".col" && ext != ".ipl" ) continue;

		auto stem = file.stem().string();
		std::transform( stem.begin(), stem.end(), stem.begin(), ::tolower );
		auto lowercase_name = stem + ext;
		if ( file.filename() == lowercase_name ) continue;

		std::filesystem::rename( file, mods_path / lowercase_name );
	}

	auto vorbis = GetModuleHandleW( L"vorbisFile.dll" );
	if ( vorbis == nullptr || vorbis == INVALID_HANDLE_VALUE ) return;

	if ( GetProcAddress( vorbis, "Logc" ) == nullptr ) return;
	arz_launcher = true;
}

AsiPlugin::~AsiPlugin() = default;

void AsiPlugin::CdStreamOpen( const char *&archive ) {
	cur_archive = archive;

	if ( !cur_archive.has_extension() ) return;
	if ( cur_archive.extension() != ".IMG" && cur_archive.extension() != ".img" ) return;

	cur_mod_archive = mods_path / cur_archive.filename();

	static std::set<std::size_t> already_modified;
	auto hash =
		std::hash<std::string_view>{}( archive ); // Use std::string_view, because hash for const char* - cast pointer to std::size_t
	if ( !already_modified.contains( hash ) )
		already_modified.insert( hash );
	else if ( !arz_launcher ) {
		mod_archive_path = cur_mod_archive.string();
		archive = mod_archive_path.c_str();
		return;
	}

	ImgDirectory::open( cur_archive, [this]( DirectoryInfo &entry, std::ifstream &archive ) {
		process_archive( entry, archive );
		return true;
	} );

	cur_img.reset( nullptr );
	if ( !arz_launcher ) {
		mod_archive_path = cur_mod_archive.string();
		archive = mod_archive_path.c_str();
	}
}

void AsiPlugin::process_archive( DirectoryInfo &entry, std::ifstream &archive ) {
	std::transform( std::begin( entry.name ), std::end( entry.name ), std::begin( entry.name ), ::tolower );
	std::string_view entry_name = entry.name;

	if ( !entry_name.ends_with( ".dff" ) && !entry_name.ends_with( ".txd" ) && !entry_name.ends_with( ".col" ) &&
		 !entry_name.ends_with( ".ipl" ) )
		return;

	auto mod_file = mods_path / entry.name;
	if ( !std::filesystem::exists( mod_file ) ) {
		if constexpr ( kRestoreDeletedFiles ) {
			if ( !std::filesystem::exists( cur_mod_archive ) ) return;
			if ( !has_contain_file( cur_mod_archive, entry_name ) ) return;

			// Restore original file
			auto img = open_cur_img();
			auto it = std::find_if( img->entries.begin(), img->entries.end(), [&entry_name]( const ImgEntry &ent ) {
				auto name = ent.name().string();
				std::transform( name.begin(), name.end(), name.begin(), ::tolower );
				return name == entry_name;
			} );
			if ( it == img->entries.end() ) {
				std::cerr << PROJECT_NAME << ": Missing file " << std::quoted( entry_name ) << " in modded archive" << std::endl;
				return;
			}
			std::size_t size = entry.size << 11;
			auto data = std::make_unique<char[]>( size );
			archive.seekg( entry.offset << 11 );
			archive.read( data.get(), size );
			*it = std::span{ reinterpret_cast<std::uint8_t *>( data.get() ), size };
			cur_img->modified = true;
		}
		return;
	}

	auto img = open_cur_img();
	auto it = std::find_if( img->entries.begin(), img->entries.end(), [&entry_name]( const ImgEntry &ent ) {
		auto name = ent.name().string();
		std::transform( name.begin(), name.end(), name.begin(), ::tolower );
		return name == entry_name;
	} );
	std::size_t size = std::filesystem::file_size( mod_file );
	auto data = std::make_unique<char[]>( size );
	std::ifstream f( mod_file, std::ios::binary );
	f.read( data.get(), size );
	f.close();
	if ( it == img->entries.end() ) {
		img->entries.emplace_back( entry_name, std::span{ reinterpret_cast<std::uint8_t *>( data.get() ), size } );
		cur_img->modified = true;
	} else {
		auto span_data = std::span{ reinterpret_cast<std::uint8_t *>( data.get() ), size };
		if ( size % 2048 != 0 || std::memcmp( span_data.data(), it->data().data(), span_data.size() ) != 0 ) {
			*it = span_data;
			cur_img->modified = true;
		}
	}

	if constexpr ( !kRestoreDeletedFiles ) {
		std::cout << PROJECT_NAME << ": Install file " << std::quoted( entry_name ) << std::endl;
		std::filesystem::remove( mod_file );
	}
}

bool AsiPlugin::has_contain_file( const std::filesystem::path &archive, std::string_view file ) {
	return !ImgDirectory::open( archive, [&file]( DirectoryInfo &entry, std::ifstream &archive ) { return file != entry.name; } );
}
ImgDirectory *AsiPlugin::open_cur_img() {
	if ( !std::filesystem::exists( cur_mod_archive ) ) std::filesystem::copy_file( cur_archive, cur_mod_archive );
	if ( cur_img == nullptr || cur_img->img == nullptr ) cur_img = std::make_unique<cur_img_t>( cur_mod_archive );
	return cur_img->img.get();
}
