#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>

#include <filesystem>
#include <iomanip>
#include <iostream>

#include "SRHook.hpp"

#define DirectoryInfoStruct
class CDirectory {
public:
	struct DirectoryInfo {
		std::uint32_t offset;
		std::uint16_t size;
		std::uint16_t pad;
		char name[24];
	};
};
using DirectoryInfo = CDirectory::DirectoryInfo;
#include "img/ImgDirectory.h"
#undef DirectoryInfoStruct

class AsiPlugin : public SRDescent {
#ifdef UPDATE_IMG_BY_FILES
	static constexpr auto kRestoreDeletedFiles = true;
#else
	static constexpr auto kRestoreDeletedFiles = false;
#endif

	SRHook::Hook<const char *> CdStreamOpen_{ 0x004067b0, 5 };
	std::filesystem::path mods_path;
	std::filesystem::path cur_archive;
	std::filesystem::path cur_mod_archive;
	std::string mod_archive_path;
	bool arz_launcher = false;

	struct cur_img_t {
		cur_img_t() = delete;
		cur_img_t( const std::filesystem::path &file ) { img = std::make_unique<ImgDirectory>( file ); }
		~cur_img_t() {
			if ( modified ) {
				try {
					if ( !img->write() ) throw std::runtime_error( "libimg can't store IMG" );
					std::cout << PROJECT_NAME << ": Update IMG archive " << std::quoted( img->name ) << std::endl;
				} catch ( std::exception &e ) {
					std::cerr << PROJECT_NAME << ": Can't update IMG archive " << std::quoted( img->name ) << " - " << e.what()
							  << std::endl;
					auto name = img->name;
					for ( std::size_t i = 0;; ++i ) {
						img->name = name + '(' + std::to_string( i ) + ')';
						try {
							if ( !img->write() ) throw std::runtime_error( "libimg can't store IMG" );
							std::cout << PROJECT_NAME << ": Store backup of IMG archive " << std::quoted( name ) << " to "
									  << std::quoted( img->name ) << std::endl;
							break;
						} catch ( std::exception &e ) {}
					}
				}
			}
		}

		std::unique_ptr<ImgDirectory> img = nullptr;
		bool modified = false;
	};
	std::unique_ptr<cur_img_t> cur_img = nullptr;

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	void CdStreamOpen( const char *&archive );
	void process_archive( DirectoryInfo &entry, std::ifstream &archive );

	ImgDirectory *open_cur_img();

	static bool has_contain_file( const std::filesystem::path &archive, std::string_view file );
};

#endif // MAIN_H
