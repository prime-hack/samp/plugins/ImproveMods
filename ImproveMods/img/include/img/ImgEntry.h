#ifndef IMGENTRY_H
#define IMGENTRY_H

#include <cstdint>
#include <filesystem>
#include <fstream>
#include <memory>
#include <span>
#include <tuple>

#if __has_include("GameStructs/Directory.h")
#include "GameStructs/Directory.h"
#elif !defined(DirectoryInfoStruct)
struct DirectoryInfo;
#endif

class ImgEntry
{
	char name_[24]{0};
	std::uint32_t size_;
	std::uint32_t compsize_ = 0;
	std::unique_ptr<std::uint8_t[]> data_;

	bool compress_ = false;
	bool read_only_ = false;

public:
	ImgEntry(const std::filesystem::path &name, const std::span<std::uint8_t> &data);
	ImgEntry(const std::filesystem::path &name, std::span<std::uint8_t> &&data);
	ImgEntry(DirectoryInfo& info, std::ifstream& stream);

	ImgEntry &operator=(const std::span<std::uint8_t> &data);
	ImgEntry &operator=(std::span<std::uint8_t> &&data);

	/// Get entry name
	std::filesystem::path name() const;
	/**
	 * @brief Rename entry
	 * @details Length limit is 24 chars
	 * @param name Name
	 */
	void rename(const std::filesystem::path &name);

	/// Get loaded data
	const std::span<const uint8_t> data() const;
	/**
	 * @brief Get loaded data
	 * @details Throw @ref std::runtime_error for read-only data
	 */
	std::span<std::uint8_t> data();

	/// Get size of loaded data
	std::uint32_t size() const;
	/// Get compressed size of loaded data
	std::uint32_t compsize() const;

	/// Check is loaded data compressed
	bool is_compressed() const;
	/// Check is loaded data read-only
	bool is_readonly() const;

	/// Check is required compresssion
	bool has_need_compress() const;
	/// Enable require compression
	void compress();
	/// Disable require compression
	void uncompress();
};

#endif // IMGENTRY_H
