#ifndef IMGDIRECTORY_H
#define IMGDIRECTORY_H

#include <functional>
#include <vector>
#include <string>

#include "ImgEntry.h"

enum class eImgVer {
	VER1, // III/VC
	VER2, // SA
	VER_BOTH // SA with III/VC compatible
};

/**
 * @todo write docs
 */
class ImgDirectory
{
	std::filesystem::path path_;
public:
	ImgDirectory(eImgVer ver);
	ImgDirectory(const std::filesystem::path &path);
	~ImgDirectory();

	using ArchiveCB = std::function<bool(DirectoryInfo & /*entry*/, std::ifstream & /*archive*/)>;
	static bool open(const std::filesystem::path &path, const ArchiveCB &open_cb);

	bool write();

	eImgVer version;
	std::vector<ImgEntry> entries;
	std::string name;

	std::filesystem::path path() const;
	bool setPath(const std::filesystem::path &path);
	bool setPath(std::filesystem::path &&path);

	std::size_t size() const;
	std::vector<ImgEntry>::iterator split_by_size(std::size_t size);

protected:
	static bool do_open(std::ifstream &img, std::ifstream &dir, const ImgDirectory::ArchiveCB& open_cb);
	static bool do_open(std::ifstream &img, const ImgDirectory::ArchiveCB& open_cb);

	void do_close(std::ofstream &img, std::ofstream &dir);
	bool do_close(std::ofstream &img);
};

#endif // IMGDIRECTORY_H
